// Load express library

var express = require("express");

// Create an application

var app = express();

// Load express-handlebars

var handlebars = require("express-handlebars");

// Ask our app to use handlebars as the template engine
// Default layout to use is main (main.handlebars)
app.engine("handlebars", handlebars({
    defaultLayout: "main",
    helpers: {
        isActive: function (view, actual) {
            if (view == actual);
            return ("active");
            return ("");
        }
    }
})
);
app.set ("view engine", "handlebars");

// Initialise MySQL

var mysql = require("mysql");

// Load my dbconfig

var config = require("./my_modules/dbconfig");

var pool = mysql.createPool(config);

app.get("/", function(req, res) {
    console.info(">>> %s", req.originalUrl);
    res.render("index");
});

// Get menu/index;
app.get("/menu/:category", function(req, res) {
    console.info(">>> %s", req.params.category);
    var cat = req.params.category;
    switch (cat) {
        case ("insiders"):
        case("about"):
            res.render(cat, {
                currentDate: (new Date()).toString()
            });
            break;
        case ("visitors"):

            // Get a connection from the pool
            pool.getConnection(function(err, conn) {
                //If there are errors, report it
                if (err) {
                    console.error("get connection error: %s", err);
                    res.render("/index");
                    return;
                }
                //Perform the query
                conn.query("select * from visitors", function (err, rows) {
                    if(err) {
                        console.error("query has error: %s", err);
                        res.render("/index");
                        return;
                    }
                    //Render the template with rows as the context
                    res.render("visitors", {
                        visitors: rows
                    });
                    //IMPORTANT! Release the connection
                    conn.release();
                });
            });
            break;
        
        default:
            res.redirect("/")
            break;
    }
});

// Process incoming request

app.get("/search", function (req,res) {
    console.info("Searching for.... %s", req.query.visitorDetails);
    pool.getConnection(function(err, conn) {
        //If there are errors, report it
        if (err) {
            console.error("get connection error: %s", err);
            res.render("/menu/visitors");
            return;
        }
        //Perform the query
        
        var q = "select * from visitors where vname like " + "'%" + req.query.visitorDetails + "%';" + "or (vemail like " + "'%" + req.query.visitorDetails + "%')" + "or (vcounty like " + "'%" + req.query.visitorDetails + "%')" + "or (vcity like " + "'%" + req.query.visitorDetails + "%');"
        var paramQuery = "select * from customers where name like ?";
        
        conn.query(paramQuery, //query
            ["%" + req.query.namequery + "%"], //actual parameters based on position 
            function (err, rows) {
            if(err) {
                console.error("query has error: %s", err);
                res.render("/menu/visitors");
                return;
            }
            //Render the template with rows as the context
            res.render("visitors", {
                visitors: rows
            });
            //IMPORTANT! Release the connection
            conn.release();
        });
    });

        });

// Set the public/static directory
// Create a landing page in the public directory

app.use(express.static(__dirname + "/public"));

// Catchall (error handler)
app.use(function(req, res, next) {
    console.error("File not found: %s", req.originalUrl);
    res.redirect("/");
});

// Start the server

app.listen(3000, function() {
    console.info("Application started on port 3000")
});
