angular.module('routingApp', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/', {
            template: '<h2> Home </h2>'
        })

            .when('/visitors', {
                template: '<h2> Visitors </h2>'
            })

            .when('/insiders', {
                template: '<h2> Insiders </h2>'
            })

            .when('/about', {
                template: '<h2> About </h2>'
            })

            .otherwise({})
    }
